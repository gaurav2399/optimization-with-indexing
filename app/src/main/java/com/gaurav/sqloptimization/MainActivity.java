package com.gaurav.sqloptimization;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.util.Log;

import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    DatabaseHelper databaseHelper = new DatabaseHelper(this);
    DatabaseContract databaseContract = new DatabaseContract();

    String sql;
    Cursor cursor;
    ArrayList emailList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        String[] names = {"gaurav", "aayush", "yug", "abc", "defg", "badal", "aakash", "heelo", "world", "bye", "unknown", "kya", "kru"};

        /******** WITH TRANSACTION AND INDEXING ********/

        long time1s = System.currentTimeMillis();

        try {
            Random r = new Random();
            int i = 0;
            db.beginTransaction();
            sql = "INSERT INTO " + databaseContract.contacts_details_index + "( name, email, phone, salary )" + " VALUES (?, ?, ?, ?)";
            SQLiteStatement statement = db.compileStatement(sql);
            while (i++ < 50000) {
                StringBuilder email = new StringBuilder();
                int j = 0;
                while (j++ < 20) {
                    int x = r.nextInt(26);
                    email.append((char) (x + 97));
                }
                String name = names[r.nextInt(13)];
                StringBuilder phone = new StringBuilder();
                j = 0;
                while (j++ < 10) {
                    int y = r.nextInt(10);
                    phone.append(y);
                }
                email.append("@gmail.com");
                Log.d(TAG, "email: " + email + " name: " + name + " phone: " + phone);
                statement.clearBindings();
                statement.bindString(1, name);
                statement.bindString(2, String.valueOf(email));
                statement.bindString(3, String.valueOf(phone));
                statement.bindString(4, "100000");
                statement.executeInsert();
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, "exception is " + e.getMessage());
        } finally {
            db.endTransaction();
        }

        long time1e = System.currentTimeMillis();
        Log.e(TAG, "insertion time " + (time1e - time1s));

        if (isIndexExists()) Log.e(TAG, "after insertion index exists");
        else Log.e(TAG, "after insertion index not exists");


        /************** READING WITHOUT INDEXING ***************/


        // Ist Query

        long time22s = System.currentTimeMillis();
        sql = "Select * from " + databaseContract.contacts_details_index + " where " + databaseContract.name + " = 'gaurav'";
        cursor = db.rawQuery(sql, null);
        emailList = new ArrayList<>();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String email = cursor.getString(cursor.getColumnIndex(databaseContract.email));
                emailList.add(email);
            } while (cursor.moveToNext());
        }
        cursor.close();
        long time22e = System.currentTimeMillis();

        Log.e(TAG,"query is " + sql);
        Log.e(TAG,"elapse time while getting email without indexing is" + (time22e - time22s));
        Log.e(TAG,"list size is" + emailList.size());

        // 2nd Query

        long time2s = System.currentTimeMillis();
        sql = "Select * from " + databaseContract.contacts_details_index + " where " + databaseContract.name + " = 'aayush'";
        cursor = db.rawQuery(sql, null);
        emailList = new ArrayList<>();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String email = cursor.getString(cursor.getColumnIndex(databaseContract.email));
                emailList.add(email);
            } while (cursor.moveToNext());
        }
        cursor.close();
        long time2e = System.currentTimeMillis();

        Log.e(TAG,"query is " + sql);
        Log.e(TAG,"elapse time while getting email without indexing is: 2 query " + (time2e - time2s));
        Log.e(TAG,"list size is: 2 query " + emailList.size());

        if(isIndexExists()) Log.e(TAG,"index exists");
        else Log.e(TAG,"index not exists");

        // make index
        db.execSQL(databaseHelper.index_on_email);


        /********* CHECKING INDEXING IS DONE BY SERVER OR NOT *************/

        sql = "explain query plan select * from " + databaseContract.contacts_details_index + " where name = 'gaurav'";

        if(isIndexExists()) Log.e(TAG,"indexing exists");
        else Log.e(TAG,"indexing not exists");

        cursor = db.rawQuery(sql, null);
        if(cursor!=null) {
            if(cursor.getCount()>0) {
                Log.e(TAG,"cursor count is " + cursor.getCount() + " and column count is " + cursor.getColumnCount());
                cursor.moveToFirst();
                String detail = cursor.getString(cursor.getColumnIndex("detail"));
                Log.e(TAG,"cursor val " + detail);
                cursor.close();
            }
        }

        if(isIndexExists()) Log.e(TAG,"index exists");
        else Log.e(TAG,"index not exists");

        /************** READING AFTER INDEXING ***************/


        // Ist Query
        long time4s = System.currentTimeMillis();
        sql = "Select * from " + databaseContract.contacts_details_index + " where " + databaseContract.name + " = 'gaurav'";
        cursor = db.rawQuery(sql, null);
        emailList = new ArrayList<>();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String email = cursor.getString(cursor.getColumnIndex(databaseContract.email));
                emailList.add(email);
            } while (cursor.moveToNext());
        }
        cursor.close();
        long time4e = System.currentTimeMillis();

        Log.e(TAG,"query is " + sql);
        Log.e(TAG,"elapse time while getting email with indexing is " + (time4e - time4s));
        Log.e(TAG,"list size is " + emailList.size());


        // 2nd Query
        long time44s = System.currentTimeMillis();
        sql = "Select * from " + databaseContract.contacts_details_index + " where " + databaseContract.name + " = 'aayush'";
        cursor = db.rawQuery(sql, null);
        emailList = new ArrayList<>();
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String email = cursor.getString(cursor.getColumnIndex(databaseContract.email));
                emailList.add(email);
            } while (cursor.moveToNext());
        }
        cursor.close();
        long time44e = System.currentTimeMillis();

        Log.e(TAG,"query is " + sql);
        Log.e(TAG,"elapse time while getting email with indexing is: 2 query " + (time44e - time44s));
        Log.e(TAG,"list size is: 2 query " + emailList.size());

    }

    // check only table not confirm indexing
    public boolean isIndexExists() {
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        String query = "SELECT * FROM sqlite_master WHERE type= 'index';";
        try {
            Cursor cursor = db.rawQuery(query, null);
            if(cursor!=null) {
                if(cursor.getCount()>0) {
                    cursor.close();
                    return true;
                }
            }
            return false;
        }catch (Exception e){
            Log.e(TAG,"exception is " + e.getMessage());
        }
        return false;
    }
}