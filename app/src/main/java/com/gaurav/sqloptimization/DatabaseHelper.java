package com.gaurav.sqloptimization;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "salaryDatabase10";
    public static final String TAG = DatabaseHelper.class.getSimpleName();

    DatabaseContract databaseContract = new DatabaseContract();

    public Context mContext;

    public String ma_contacts_details = "Create table IF NOT EXISTS " + databaseContract.contacts_details + "(" +
            databaseContract.id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            databaseContract.name + " TEXT , " +
            "" + databaseContract.email+ " TEXT," +
            "" + databaseContract.phone + " TEXT," +
            "" + databaseContract.salary + " INTEGER);" ;

    public String ma_contacts_details_index = "Create table IF NOT EXISTS " + databaseContract.contacts_details_index + "(" +
            databaseContract.id + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            databaseContract.name + " TEXT , " +
            "" + databaseContract.email+ " TEXT," +
            "" + databaseContract.phone + " TEXT," +
            "" + databaseContract.salary + " INTEGER);" ;

    public String index_on_email = "Create index " + databaseContract.index_email + " on " +
            databaseContract.contacts_details_index + " (" + databaseContract.name + ");";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e(TAG,"on create");
        db.execSQL(ma_contacts_details);
        db.execSQL(ma_contacts_details_index);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e(TAG,"on upgrade");
        db.execSQL("Drop table if exists " + databaseContract.contacts_details);
        db.execSQL("drop table if exists " + databaseContract.contacts_details_index);
        db.execSQL("drop index if exists " + databaseContract.index_email);
        onCreate(db);
    }
}
