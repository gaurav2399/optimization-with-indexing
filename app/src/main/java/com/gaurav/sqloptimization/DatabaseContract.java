package com.gaurav.sqloptimization;

public class DatabaseContract {

    public DatabaseContract(){}

    public static final String contacts_details = "contacts_details";
    public static final String id = "id";
    public static final String contacts_details_index = "contacts_details_index";
    public static final String index_email = "index_email";
    public static final String name = "name";
    public static final String email = "email";
    public static final String phone = "phone";
    public static final String salary = "salary";
}
